var expect = require('chai').expect;
var { Builder, By, Key, until } = require('selenium-webdriver');


describe('Test the HTML form', () => {
  const driver = new Builder()
    //.forBrowser('chrome')
  // Sets the URL of a remote WebDriver server to use
    // "selenium" is the dokcer container name 
    .usingServer("http://localhost:4444/wd/hub")
    // Setting capabilities when requesting a new session
    .withCapabilities({"browserName":"chrome"})
    // Creates a new WebDriver client based on this builder's current configuration. 
    .build();
  // Describe a specification or test-case with the given title and callback
  it('should add two numbers and show the result', async () => {

      // navigate to the given URL
      await driver.get('http://68.183.57.185:3000/');
      //  findElement() -> find an element on the page
      //  sendKeys() -> type a sequence on the DOM element
      // This case enter number 5 and press enter 
      await driver.findElement(By.name('num1')).sendKeys('5', Key.ENTER);
      await driver.findElement(By.name('num2')).sendKeys('10', Key.ENTER);
      // click() -> click on this elemen
      await driver.findElement(By.id('add')).click();
      //await driver.wait(until.elementTextIs(By.id('output') , '15' ), 10000)
      // getText() -> Get the visible (i.e. not hidden by CSS) innerText of this element, 
      // including sub-elements, without any leading or trailing whitespace
      const output = await driver.findElement(By.id('output')).getText();
      // test if the innerText element with the ID of "output" is equal to 15
      expect(output).to.equal('15');
   
  });
  
  // [bdd, qunit, tdd] Describe a "hook" to execute the given callback
  // quit() -> quit the current session
  after(async () => driver.quit());
});


 describe('Test the HTML form', () => {
   const driver = new Builder()
    .usingServer("http://localhost:4444/wd/hub")
    .withCapabilities({"browserName":"chrome"})
    .build();

  it('testing invalid input and get 400 static code ', async () => {

    await driver.get('http://68.183.57.185:3000/');
    await driver.findElement(By.name('num1')).sendKeys('hj', Key.ENTER);
    await driver.findElement(By.name('num2')).sendKeys('hn', Key.ENTER);
    await driver.findElement(By.id('add')).click();
    const output = await driver.findElement(By.className('err')).getText();
    expect(output).to.equal('invalid input');
   
  });

  after(async () => driver.quit());
});   